package com.chill0n.mmotester.shmem;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Vector;

import com.chill0n.mmotester.chat.ChatData;
import com.chill0n.mmotester.dbc.DBCController;
import com.chill0n.mmotester.player.PlayerCoordinate;
import com.chill0n.mmotester.player.PlayerState;
import com.chill0n.mmotester.player.PlayerData;
import com.chill0n.mmotester.player.PlayerView;
import com.chill0n.mmotester.player.SmallPlayerClass;
import com.chill0n.mmotester.world.Block;

public class SharedResManager {
	/*
	  
	 
	//Make Sure To Encode PlayerCoordinate Destionations With PLIDS
	private static Vector<Long> PlayerPLIDMap;
	private static Vector<PlayerCoordinate> CoordinateList;
	private static Vector<PlayerView> ViewList; //Same Reference As Coordinate List
	private static Vector<PlayerData> PlayerDataMap;
	private static Vector<Block> BlockData; //Use Block IDS Not Random Assignment IDS
	private static Vector<ChatData> ChatList;
	public static void SynchronizeMemoryMap_USEONLYONCE(){
		PlayerPLIDMap = new Vector<Long>();
		CoordinateList = new Vector<PlayerCoordinate>();
		ViewList = new Vector<PlayerView>();
		PlayerDataMap = new Vector<PlayerData>();
		BlockData = new Vector<Block>();
		ChatList = new Vector<ChatData>();
	}
	public static void BuildBlockMemoryMap_USEONLYONCE(){
		try { //Run After SynchronizeMemoryMap
			System.out.println("[Info]Preparing And Loading Block Metadata Block Table, Entries:"+
					new DBCController().GetTotalNumOfBlocks());
			for(int i=0;i<(new DBCController().GetTotalNumOfBlocks());i++){
				BlockData.add(new DBCController().DownloadBlock(i+1));
			}// Well that was easy
			System.out.println("[Info]Processed Block Requests ["+BlockData.size()+"]");
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			//Im going to allow this since this is only ran at startup:>
			e.printStackTrace();
		}
	}
	@Deprecated
	public static synchronized boolean JoinGame(PlayerCoordinate PlCoord, PlayerData PlData, ChatData ChatInfo){
		int CheckMap[] = new int[10];
		PlayerPLIDMap.add(PlData.getPlid());
		CheckMap[0] = PlayerPLIDMap.size();
		UpdatePlayerDeltaCoor(PlData.PlayerID,PlCoord);
		CheckMap[1] = PlayerPLIDMap.size();
		PlayerDataMap.add(PlData); //This Is All Asynchronous So We Can Assume The Operation Is 
		CheckMap[2] = PlayerPLIDMap.size();
		ChatList.add(ChatInfo); //Not Paralell, If It Was We Would Be Fucked
		CheckMap[3] = PlayerPLIDMap.size();
		if(CheckMap[0]==CheckMap[1] && CheckMap[1]==CheckMap[2] && CheckMap[0]==CheckMap[3]){
			BlockData.get(PlayerDataMap.get(PlayerPLIDMap.indexOf(PlData.PlayerID)).getBlock()).NeedsFullUpdate=true;
			return true; //Were All Good Here Since The Map Is Not Malformed
		}
		else{return false;}
	}
	public static synchronized void UpdatePlayerState(long plid ,PlayerState plstate){
		BlockData.get(PlayerDataMap.get(PlayerPLIDMap.indexOf(plid)).getBlock()).NeedsUpdate=true;
	}
	public static synchronized ArrayList<PlayerCoordinate> GetBlockDeltas(int blockid){
		ArrayList<PlayerCoordinate> ReturnCoordinates = new ArrayList<PlayerCoordinate>();
		for(int i=0;i<=BlockData.get(blockid).PlayerIDs.size();i++){
			long TempPlid = BlockData.get(blockid).PlayerIDs.get(i);
			PlayerCoordinate MainData = CoordinateList.get(PlayerPLIDMap.indexOf(TempPlid));
			ReturnCoordinates.add(MainData);
		}
		return ReturnCoordinates;
	} //This Ones For Views
	public static synchronized ArrayList<PlayerView> GetBlockViews(int blockid){
		ArrayList<PlayerView> ReturnCoordinates = new ArrayList<PlayerView>();
		for(int i=0;i<=BlockData.get(blockid).PlayerIDs.size();i++){
			long TempPlid = BlockData.get(blockid).PlayerIDs.get(i);
			PlayerView MainData = ViewList.get(PlayerPLIDMap.indexOf(TempPlid));
			ReturnCoordinates.add(MainData);
		}
		return ReturnCoordinates;
	}
	public static synchronized ArrayList<SmallPlayerClass> GetSmallPlayers(){
		ArrayList<SmallPlayerClass> ReturnClass = new ArrayList<SmallPlayerClass>();
		for(int i=0;PlayerDataMap.size()>=i;i++){
			ReturnClass.add(new SmallPlayerClass(PlayerDataMap.get(i).PlayerID, PlayerDataMap.get(i).PlayerName));
		} //Remember Set To Null This Could Really Fuck Up The Memory Frames
		return ReturnClass;
	}
	public static synchronized void UpdatePlayerDeltaCoor(long plid, PlayerCoordinate delta_coord){
		CoordinateList.set(PlayerPLIDMap.indexOf(plid), delta_coord);
		BlockData.get(PlayerDataMap.get(PlayerPLIDMap.indexOf(plid)).getBlock()).NeedsUpdate=true;
		//We Just Update The Map To Clarify The Some New Shiny Deltas
	}
	public static synchronized void UpdatePlayerViews(long plid, PlayerView player_view){
		ViewList.set(PlayerPLIDMap.indexOf(plid), player_view);
		BlockData.get(PlayerDataMap.get(PlayerPLIDMap.indexOf(plid)).getBlock()).NeedsViewUpdate = true;
		//Odly Enough The Only Difference Is The Container And Update Flag
	}
	//TODO Optomize Plid References Using Fixed Magnitude References
	public static synchronized boolean checkUpdateState(long plid){
		return BlockData.get(PlayerDataMap.get(PlayerPLIDMap.indexOf(plid)).getBlock()).NeedsUpdate;
	}//State Updates Must Be Contiguous As After 5 Read Cycles The RRState Is Erased
	public static synchronized void setUpdateState(long plid, boolean state){
		BlockData.get(PlayerDataMap.get(PlayerPLIDMap.indexOf(plid)).getBlock()).NeedsUpdate = state;
	}
	public static synchronized boolean getViewUpdateState(long plid){
		return BlockData.get(PlayerDataMap.get(PlayerPLIDMap.indexOf(plid)).getBlock()).NeedsViewUpdate;
	}
	public static synchronized void addChatMessage(ChatData Message){
		ChatList.add(Message);
	}
	public static synchronized ChatData checkChatMessageChange(){
		ChatData ChReturn = ChatList.get(ChatList.size());
		return ChReturn;
	}
	public static synchronized ArrayList<PlayerData> getFullBlockData(long plid){
		ArrayList<Long> PlayerList = new ArrayList<Long>();
		ArrayList<PlayerData> DataList = new ArrayList<PlayerData>();
		PlayerList = BlockData.get(PlayerDataMap.get(PlayerPLIDMap.indexOf(plid)).getBlock()).PlayerIDs;
		for(int i=0;i<=PlayerList.size();i++){
			DataList.add(PlayerDataMap.get((int)PlayerList.get(i).longValue())); //Stupid Java Object Reference Casts
		}
		return DataList;
	}
	public static synchronized void JoinGameNew(PlayerData PlDesc){
		PlayerPLIDMap.add(PlDesc.PlayerID);  //Yep We Need To Get EM New Player Coordinates!
		PlayerDataMap.add(PlDesc);
		CoordinateList.add(PlDesc.CurCoordinates);
		BlockData.get(PlayerDataMap.get(PlayerPLIDMap.indexOf(PlDesc.PlayerID)).getBlock()).PlayerIDs.add(PlDesc.PlayerID);
		BlockData.get(PlayerDataMap.get(PlayerPLIDMap.indexOf(PlDesc.PlayerID)).getBlock()).NeedsFullUpdate=true;
		UpdatePlayerDeltaCoor(PlDesc.PlayerID, PlDesc.CurCoordinates);
		PlayerState PlayerTemp = new PlayerState();
		PlayerTemp.RealState=PlDesc.PlayerState;
		UpdatePlayerState(PlDesc.PlayerID,PlayerTemp);
	}
	public static synchronized ArrayList<Long> getFullUpdatedPLID(int curblock){
		ArrayList<Long> ReturnList = new ArrayList<Long>();
		for(int i=0; i<=BlockData.get(curblock).PlayerIDs.size();i++){
			if(BlockData.get(curblock).isUpdateNeeded.get((int)(BlockData.get(curblock).PlayerIDs.get(i)).longValue())
					==true){
				//Ok
				ReturnList.add((BlockData.get(curblock).PlayerIDs.get(i)).longValue());
			} //Fetch Player IDS In Order And Dump In-order, This Gives A List
			else{
				//Disregard
			}
		}
		return ReturnList;
	}
	public static synchronized void setFullUpdateOnBlock(boolean update, int curblock, long plid){
		BlockData.get(curblock).NeedsFullUpdate = update;
		BlockData.get(curblock).isUpdateNeeded.set(BlockData.get(curblock).PlayerIDs.indexOf(plid), true);
	}
	public static synchronized PlayerData getPlayerRawData(long plid){
		PlayerData PlDataPointer = PlayerDataMap.get(PlayerPLIDMap.indexOf(plid));
		return PlDataPointer;
	}
	*/
}
