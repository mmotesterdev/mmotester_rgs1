package com.chill0n.mmotester.state;

import java.util.ArrayList;

import com.chill0n.mmotester.net.Packet;
import com.chill0n.mmotester.state.managers.ChatStateManager;
import com.chill0n.mmotester.state.managers.DeltaStateManager;
import com.chill0n.mmotester.state.managers.PlayerStateManager;
import com.chill0n.mmotester.thread.PlayerThread;

public class StateManager {
	public static final int StateMGRMax = 1024;
	public static final int ReturnTableSize = 2;
	private DeltaStateManager DeltaManager;
	private PlayerThread PlHandle;
	private ChatStateManager ChatManager;
	private PlayerStateManager PlayerManager;
	public StateManager(PlayerThread SystemTable){//We Need A Player Thread Handle TO Gain Access
		PlHandle = SystemTable;
		DeltaManager = new DeltaStateManager();
		ChatManager = new ChatStateManager();
		PlayerManager = new PlayerStateManager();
	}
	public ArrayList<ArrayList<Packet>> ProcessVector(){
		ArrayList<ArrayList<Packet>> ReturnTable = new ArrayList<ArrayList<Packet>>();
		ReturnTable.add(DeltaManager.CheckDelta(PlHandle));
		ReturnTable.add(ChatManager.checkChatManager(PlHandle));
		ReturnTable.add(PlayerManager.CheckPlayer(PlHandle));
		return ReturnTable;
	}
	public int getReturnSize(){
		return ReturnTableSize;
	}
}
