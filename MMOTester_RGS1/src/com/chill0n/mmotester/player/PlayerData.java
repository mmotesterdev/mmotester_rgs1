/**
 * 
 */
package com.chill0n.mmotester.player;

import java.net.UnknownHostException;
import java.util.ArrayList;

import com.chill0n.mmotester.constants.Constants;
import com.chill0n.mmotester.dbc.DBCController;

/**
 * @author nwhiteis
 *
 */
public class PlayerData {
	public PlayerCoordinate CurCoordinates;
	public int CurBlock;
	public String PlayerName;
	public final long PlayerID;
	public int PlayerLevel, PlayerClass, PlayerTCPPort, PlayerUDPPort; //UDP/TCP Must Be Seperate In TEMP
	public ArrayList<Long> OwnedBlocks;
	public double CurrentMoney;
	public ArrayList<Integer> OwnedItems;
	public long PlayerGuild;
	public int PlayerState;
	public PlayerData(long plid, PlayerCoordinate curcoordinate, int curblock, String playername, int playerlevel,
			int playerclass, int playertcpport, int playerudpport, ArrayList<Long> ownedblocks,
			double currentmoney, ArrayList<Integer>owneditems, long playerguild, int playerstate){
		CurCoordinates = curcoordinate;
		CurBlock = curblock;
		PlayerName = playername;
		PlayerID = plid;
		PlayerLevel = playerlevel;
		PlayerClass = playerclass;
		PlayerTCPPort = playertcpport;
		PlayerUDPPort = playerudpport;
		OwnedBlocks = ownedblocks;
		CurrentMoney = currentmoney;
		OwnedItems = owneditems;
		PlayerGuild = playerguild;
		PlayerState = playerstate;
	}
	public void RePopulateFromDB() throws UnknownHostException{ //TODO REMOVE METHOD FINAL POPULATION STATE INVALID!
		DBCController MainController = new DBCController();
		//MainController.GatherLogin(PlayerID, PlayerRandomKey);
	}
	public long getPlid(){
		return PlayerID;
	}
	public int getBlock(){
		return CurBlock;
	}
}
