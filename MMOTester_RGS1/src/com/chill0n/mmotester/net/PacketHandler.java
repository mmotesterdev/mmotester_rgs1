package com.chill0n.mmotester.net;

import java.io.IOException;
import java.util.Vector;

import com.chill0n.mmotester.handler.HandlerInitalization;

public class PacketHandler {
	private NetManager NetM;
	private HandlerInitalization Hinit;
	public PacketHandler(NetManager NetMGR){
		NetM = NetMGR;
		Hinit = new HandlerInitalization(NetMGR);
	}
	public static final int KEEP_ALIVE = 0x0;
	public static final int LOGIN = 0x1;
	public static final int CHAT = 0x2;
	public static final int DELTA_UPDATE = 0x3;
	/*
	 * Up Is The Constants Which Ultimately Define Opcodes!
	 * Below It The Handler
	 */
	public void Process(Packet Proc){
		switch(Proc.PacketHeader){
		case KEEP_ALIVE:
			Hinit.KeepAliveHandle.KeepAliveMAIN(Proc);
			break;
		case LOGIN://Unused!
			Hinit.LoginHandle.DoNothingMAIN(); //WTF
			break;
		case CHAT:
			Hinit.ChatHandle.HandleChat(Proc, NetM.pthread);
			break;
		case DELTA_UPDATE:
			Hinit.DeltaUpdateHandle.DeltaUpdateMAIN(Proc, NetM.pthread);
			break;
		}
	}
}
