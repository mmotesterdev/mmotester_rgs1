package com.chill0n.mmotester.net.packet;

import java.nio.ByteBuffer;
import java.util.ArrayList;

import com.chill0n.mmotester.constants.Constants;
import com.chill0n.mmotester.net.Packet;
import com.chill0n.mmotester.net.PacketDescriptor;
import com.chill0n.mmotester.player.PlayerCoordinate;

public class DeltaUpdatePacket {
	public final int PackCode = 3; //DESC
	public final int Descriptor = 1;
	private byte ReturnPacket[] = new byte[Constants.MaxPacketSize];
	private Packet returnPacket;
	public DeltaUpdatePacket(ArrayList<PlayerCoordinate> StatusUpdate){
		//TODO Implement Error For Too Many Players, Oh It Wont Matter
		ByteBuffer InsertionBuffer = ByteBuffer.allocate(((StatusUpdate.size()*48)));//48 Bytes For Safety
		InsertionBuffer.putInt(StatusUpdate.size());
		for(int i=0;i<=StatusUpdate.size()*4;i++){
			InsertionBuffer.putLong(StatusUpdate.get(i).plid);
			InsertionBuffer.putLong(StatusUpdate.get(i).send_time);
			InsertionBuffer.putFloat(StatusUpdate.get(i).Get_X_Coord());
			InsertionBuffer.putFloat(StatusUpdate.get(i).Get_Y_Coord());
			InsertionBuffer.putFloat(StatusUpdate.get(i).Get_Z_Coord());
			InsertionBuffer.putFloat(StatusUpdate.get(i).Get_X_Delta());
			InsertionBuffer.putFloat(StatusUpdate.get(i).Get_Y_Delta());
			InsertionBuffer.putFloat(StatusUpdate.get(i).Get_Z_Delta());
		}
		returnPacket = new Packet(PackCode,new PacketDescriptor().setPacketDesc(Descriptor)
				,(short)((StatusUpdate.size()*3)*4),InsertionBuffer.array());
	}
	public Packet getPacket(){
		return returnPacket;
	}
}
