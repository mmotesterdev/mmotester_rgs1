package com.chill0n.mmotester.world;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

import com.chill0n.mmotester.chat.ChatData;
import com.chill0n.mmotester.nparchive.json.JsonObject;
import com.chill0n.mmotester.player.PlayerCoordinate;
import com.chill0n.mmotester.player.PlayerData;
/*
 * To prevent deadlocks all writing operations should take place within the 
 * residing original thread. Although reading operations to floating references
 * are free! (atleast were not on the cell broadband architecture (256kb?) Spe
 */
public class Block {
	/*Need To Synchronize "PlayerCoordinate", "PlayerData", "ChatData" */
	public Vector<Long> PlayerIDs;
	public Vector<Vector<Long>> UnloadQueue;
	public Vector<PlayerCoordinate> DeltaList;
	public Vector<PlayerData> PlayerDataList;
	public Vector<ChatData> ChatList;
	public Vector<Boolean> isUpdateNeeded;
	public Vector<Boolean> hasUpdatedModel;
	public Vector<Boolean> hasUpdatedTexture;
	public float BlockCoordX, BlockCoordY, BlockCoordZ;
	public boolean NeedsModelUpdate;
	public boolean NeedsTextureUpdate;
	public Block(float x, float y, long Owner_plid, long guild_id){
			PlayerIDs = new Vector<Long>();
			DeltaList = new Vector<PlayerCoordinate>();
			ChatList = new Vector<ChatData>();
			UnloadQueue = new Vector<Vector<Long>>();
			PlayerDataList = new Vector<PlayerData>();
			BlockCoordX = x;
			BlockCoordY= y;
	}
	public synchronized void joinBlock(PlayerData DLMap){
		PlayerDataList.add(DLMap);
		DeltaList.add(DLMap.CurCoordinates);
		ChatData ChatStruct = new ChatData();
		ChatStruct.data = "Player ["+DLMap.PlayerName+"] Has Joined The Game!";
		ChatStruct.plid = DLMap.PlayerID;
		ChatStruct.tag = "[Block]";
		ChatList.add(ChatStruct);
		PlayerIDs.add(DLMap.PlayerID);
	}
	public synchronized void LeaveBlock(PlayerData DLMap){
		PlayerDataList.remove(DLMap);
		DeltaList.remove(DLMap.CurCoordinates);
		ChatList.remove((new ChatData().plid=DLMap.PlayerID));
		PlayerIDs.remove(DLMap.PlayerID);
	}
	public synchronized ArrayList<PlayerCoordinate> PullUpdatedDeltas(){
		ArrayList<PlayerCoordinate> PullList = new ArrayList<PlayerCoordinate>();
		for(int i=0;i<DeltaList.size();i++){ //Force Pass By Copy "Copy'
			PullList.add(DeltaList.get(i));
		}
		return PullList;
	}
	public synchronized ArrayList<ChatData> PullChatData(){
		ArrayList<ChatData> PullList = new ArrayList<ChatData>();
		for(int i=0;i<ChatList.size();i++){ //Force Pass By Copy "Copy'
			PullList.add(ChatList.get(i));
		}
		return PullList;
	}
	public synchronized ArrayList<PlayerData> CheckAndOrPullPlayerUpdate(long plid){
		ArrayList<PlayerData> returnList = new ArrayList<PlayerData>();
		for(int i=0;i<UnloadQueue.size();i++){
			long getPlid = UnloadQueue.get(PlayerIDs.indexOf(plid)).get(i);
			UnloadQueue.get(PlayerIDs.indexOf(plid)).remove(i);
			returnList.add(PlayerDataList.get(PlayerIDs.indexOf(getPlid)));
		}
		return returnList;
	}
	public synchronized void PushPlayerUpdate(PlayerData update){
		for(int i=0;i<UnloadQueue.size();i++){
			UnloadQueue.get(i).add(update.PlayerID);
		}
		//We Don't Need To Reinforce The Reference Since Its Naturally Atomic
	}
	public synchronized void PushChatUpdate(ChatData ChData){
		ChatList.set(PlayerIDs.indexOf(ChData.plid),ChData);
	}
	/*
	 * Deltas Are Automatically Pushed Since Everything Is Pass By Reference In *Java*
	 * (Well kind'a of since the compiler doesn't actually resolve the stack reference symbols)
	 */
}
