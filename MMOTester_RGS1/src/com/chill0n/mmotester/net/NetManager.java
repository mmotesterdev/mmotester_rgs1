package com.chill0n.mmotester.net;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.concurrent.TimeoutException;

import javax.net.ServerSocketFactory;
import javax.net.SocketFactory;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;

import com.chill0n.mmotester.constants.Constants;
import com.chill0n.mmotester.thread.PlayerThread;

public class NetManager {
	/*
	 * NetMGR A Drop In Low Level Socket Glue Layer For MMOTester And Other MMOTester
	 * Author: Noah Whiteis
	 * Version: 0x0x0x0
	 * 
	 * Intended Workflow For NetMGR:
	 * Packets Are Constructed And Deconstructed Here For Use By The Packet Handler,
	 * Which Is Coincendtly Spawned From This Class To Handle Request;
	 */
	protected ServerSocket TCPServerSocket;
	protected Socket TCPSocket;
	protected InputStream TCPIstream;
	protected OutputStream TCPOstream;
	protected DatagramSocket UDPSocket;
	protected InetAddress ClientDatagramIP ;
	protected int ClientDatagramPort;
	protected PacketHandler PHandler;
	protected long UDPCounter;
	public PlayerThread pthread; // Not the posix kind
	public NetManager(Socket LoginSocket, long plid, PlayerThread thread){
		try{
			TCPSocket = LoginSocket;
			TCPIstream = TCPSocket.getInputStream();
			TCPOstream = TCPSocket.getOutputStream();
			{ //Wait For UDP Back Reply
				/*UDPSocket = new DatagramSocket();
				byte BlankSessionBuffer[] = new byte[4046];//
				DatagramPacket ReceivePacket = new DatagramPacket(BlankSessionBuffer,
				Constants.LoginACK.getBytes().length);
				UDPSocket.setSoTimeout(Constants.ServerListenTimeout);
				TCPOstream.write(Constants.LoginACK.getBytes());
				UDPSocket.receive(ReceivePacket);
				ClientDatagramIP = ReceivePacket.getAddress();
				ClientDatagramPort = ReceivePacket.getPort();
				UDPSocket.send(new DatagramPacket(Constants.LoginACK.getBytes(),0,Constants.LoginACK.getBytes().length,
				ClientDatagramIP, ClientDatagramPort));
								//Send Back Packet To Verify Connection!*/								return;
							}
		} catch (IOException e) {
			try {
				CleanupSockets();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			thread.ExitThread();
		}
	}
	protected byte[] getRawTCP() throws IOException{
		byte TempBuffer[] = new byte[Constants.MaxPacketSize];
		TCPIstream.read(TempBuffer);
		return TempBuffer;
	}
	public void SendTCP(Packet TCPPacket) throws IOException{
		TCPOstream.write(TCPPacket.getPacketBytes());
	}
	public Packet pollTCP() throws IOException{
		int Header, DescriptorType, IPAddr;
		short DataSize;
		byte Data[] = new byte[Constants.MaxPacketSize];
		ByteBuffer TempData = ByteBuffer.allocate(Constants.MaxPacketSize);
		byte RawData[] = new byte[Constants.MaxPacketSize]; // All Packet Buffers Should Equal The Max Size
		RawData = getRawTCP(); //Use The Native Method!
		TempData.put(RawData); //Put The Raw Data
		TempData.clear(); // Clear The Position
		Header = TempData.getInt(); //Get Integer And Increment By Four
		DescriptorType = TempData.getInt(); //Get Descriptor And Incement By 4
		DataSize = TempData.getShort(); //Get Size
		TempData.get(Data,TempData.position(),TempData.remaining()); //Move The Rest Of The Data
		Packet RetPacket = new Packet(Header,new PacketDescriptor(),DataSize,Data);
		return RetPacket; //Were Done
	}
	public void SendUDP(UDPPacket Packet) throws IOException{
		//Sends Build Packet Which Size Is Computer Based On The Datasize
		DatagramPacket buildPacket = new DatagramPacket(Packet.getPacketBytes(),Packet.PacketDataSize+10,
				ClientDatagramIP, ClientDatagramPort);
		UDPSocket.send(buildPacket); //Lets Send This MOthaFu**
	}
	public InetAddress getUDPInetAddress(){
		return ClientDatagramIP;
	}
	public int getUDPInetPort(){
		return ClientDatagramPort;
	}
	public int getTCPInetPort(){
		return TCPSocket.getPort();
	}
	public InetAddress getTCPInetAddress(){
		return TCPSocket.getInetAddress();
	}
	public void CleanupSockets() throws IOException{
		if(TCPSocket == null || TCPServerSocket == null || TCPIstream == null ||
				TCPOstream == null || UDPSocket == null){
			return; //These IO Pointers (Or Java Talk) "Objects" Do Not Equal A Active Code Segment
		}
		TCPSocket.close();
		TCPServerSocket.close();
		TCPIstream.close();
		TCPOstream.close();
		UDPSocket.close();
	}
	public void InitPacketHandler(){
		PHandler = new PacketHandler(this);
	}
	public PacketHandler getHandleClass(){
		return PHandler;
	}
	public NetMGRInfo GetNetMGRInfo(){
		return new NetMGRInfo();
	}
	public void CheckSession(String uname){
		//TODO Implement JSON HTTP Check Manager
	}
	public void VerifyCheck(){
		//TODO Implement JSON HTTP Hash Salt Verfication
	}
	public void CreatePRM(){
		//For Building PRM
	}
	public void returnBadStackTrace(){
		System.out.println(Thread.currentThread().getPriority());
	}
}
	
