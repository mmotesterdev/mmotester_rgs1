package com.chill0n.mmotester.net.packet;

import java.nio.ByteBuffer;
import java.security.NoSuchAlgorithmException;

import com.chill0n.mmotester.net.Packet;
import com.chill0n.mmotester.net.PacketDescriptor;

public class PlayerDownloadPacket extends BlockDownloadPacket{
	protected long DownloadPLID;
	public PlayerDownloadPacket(long plid, int Blockid ,byte playerkModelData[], byte playerTextureData[]) throws NoSuchAlgorithmException{
		super(Blockid, playerkModelData, playerTextureData);
		DownloadPLID = plid;
	}
	@Override
	public Packet GetDownloadPacket(){
		ByteBuffer ReturnArr = ByteBuffer.allocate(48);//48 Bits 4+4+8+16+16
		ReturnArr.putInt(ModelBlockFragments);
		ReturnArr.putInt(TextureBlockFragments);
		ReturnArr.putInt(BlockID);
		ReturnArr.putLong(DownloadPLID);
		ReturnArr.putLong(DownloadID);
		ReturnArr.put(ModelMD5Checksum);
		ReturnArr.put(TextureMD5Checksum);
		return new Packet(PackCode, new PacketDescriptor().setPacketDesc(Descriptor),
				(short)ReturnArr.array().length, ReturnArr.array());
	}
}
