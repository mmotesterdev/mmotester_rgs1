package com.chill0n.mmotester.net.packet;

import java.nio.ByteBuffer;

import com.chill0n.mmotester.net.Packet;
import com.chill0n.mmotester.net.PacketDescriptor;

public class TextureFragmentPacket {
	public final int PackCode = 9;
	public final int Descriptor = 2; //TCP
	private Packet ReturnPacket;
	public TextureFragmentPacket(long DownLoadID, int FragmentNumber, byte Data[]){
		ByteBuffer ReturnBytes = ByteBuffer.allocate(12+Data.length);
		ReturnBytes.putLong(DownLoadID);
		ReturnBytes.putInt(FragmentNumber);
		ReturnBytes.put(Data);
		ReturnPacket = new Packet(PackCode, new PacketDescriptor().setPacketDesc(Descriptor),
				(short)20001, ReturnBytes.array());
	}
	public Packet GetPacket(){
		return ReturnPacket;
	}
}
