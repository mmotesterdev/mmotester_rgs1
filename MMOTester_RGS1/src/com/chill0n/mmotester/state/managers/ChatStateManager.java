package com.chill0n.mmotester.state.managers;

import java.util.ArrayList;

import com.chill0n.mmotester.chat.ChatData;
import com.chill0n.mmotester.net.Packet;
import com.chill0n.mmotester.net.packet.ChatPacket;
import com.chill0n.mmotester.shmem.SharedResManager;
import com.chill0n.mmotester.thread.PlayerThread;

public class ChatStateManager {
	public ArrayList<Packet> checkChatManager(PlayerThread ThreadHandle){
		ArrayList<Packet> returnPackets = new ArrayList<Packet>();
		ArrayList<ChatData> ReferredData = ThreadHandle.CurrentSharedBlock.PullChatData();
		for(int i=0;i<ReferredData.size();i++){
			returnPackets.add(new ChatPacket(ReferredData.get(i).plid,
					ReferredData.get(i).tag, ReferredData.get(i).data).getPacket());
		}
		return returnPackets;
	}
}
