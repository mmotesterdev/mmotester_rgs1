package com.chill0n.mmotester.dbc;

import java.util.ArrayList;


import com.chill0n.mmotester.constants.Constants;
import com.chill0n.mmotester.player.PlayerCoordinate;
import com.chill0n.mmotester.player.PlayerData;
import com.chill0n.mmotester.world.Block;

import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.*;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import com.mongodb.MongoInterruptedException;

@SuppressWarnings( "deprecation" )
//Fuck It I Dont Want Eclipse Pushing me Since This Method Is "Deprecated"..
public class DBCController {
	//TODO Need Good Reference Implementation FOR DBC
	MongoClient mongoHandle;
	DB mongoDBHandle;
	boolean isAuthTrue;
	public DBCController() throws UnknownHostException{
		mongoHandle = new MongoClient(Constants.DBCURL, Constants.DBCPort);
		mongoDBHandle = mongoHandle.getDB(Constants.DBCName);
		//isAuthTrue = mongoDBHandle.authenticate(Constants.DBCUsrname, Constants.DBCPassword.toCharArray());
		//if(isAuthTrue!=true){ 
		//	mongoHandle.close(); 
		//}
	}
	/*long plid, PlayerCoordinate curcoordinate, int curblock, String playername, int playerlevel,
	*int playerclass, int playertcpport, int playerudpport, byte plraneomkey[], ArrayList<Long> ownedblocks,
	*double currentmoney, ArrayList<Integer>owneditems, long playerguild
	*NOSQL Handles Are Referenced Through Long
	*/
	public PlayerData GatherLogin(long plid){
		DBCollection RetCollection = mongoDBHandle.getCollection(Constants.DBCollectioName);
		/*
		 * Ret Collection Setup
		 * (BAD Gonna Try To fix That InterruptError (But Whats Causing The Interrupt????)
		*/
		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put("plid", plid);
		DBCursor MainCur =  RetCollection.find(searchQuery);
		DBObject MainObject = MainCur.next();
		PlayerCoordinate NCoords = new PlayerCoordinate(plid, System.nanoTime()/1000
				,new Float((Double)MainObject.get("x")), new Float((Double)MainObject.get("y")),
				new Float((Double)MainObject.get("z")), new Float((Double)MainObject.get("x_delta")), 
				new Float((Double)MainObject.get("y_delta")), new Float((Double)MainObject.get("z_delta")));
		DBObject Report = (DBObject) MainObject.get("blocksowned");
		ArrayList<Long> OwnedBlocks = new ArrayList<Long>();
		for(int i=0;i<(Integer)Report.get("numberofblocksowned");i++){
			OwnedBlocks.add(new Long((Integer)Report.get(String.valueOf(i))).longValue());
		}
		DBObject ItemsReport = (DBObject) MainObject.get("owneditems");
		ArrayList<Integer> OwnedItems = new ArrayList<Integer>();
		for(int i=0;i<(Integer)ItemsReport.get("numberofitemsowned");i++){
			OwnedItems.add((Integer)ItemsReport.get(String.valueOf(i)));
		}
		PlayerData PlHandle = new PlayerData(new Long((Integer)MainObject.get("plid")), NCoords, (Integer)MainObject.get("curblock"), 
				(String)MainObject.get("playername"), (Integer)MainObject.get("playerlevel"), (Integer)MainObject.get("playerclass"), 
				(Integer)MainObject.get("playertcpport"), (Integer)MainObject.get("playerudpport") , OwnedBlocks, 
				new Float((Double)MainObject.get("currentmoney")), OwnedItems, (Integer)MainObject.get("playerguild"),  (Integer)MainObject.get("playerstate"));
		return PlHandle;
	}
	public Long CheckLogin(String HashedPassword, String Username){
		DBCollection RetCollection = mongoDBHandle.getCollection(Constants.DBCollectioName);
		/*
		 * Ret Collection Setup
		 * (BAD Gonna Try To fix That InterruptError (But Whats Causing The Interrupt????)
		*/
		try{
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put("playerloginname", Username);
			DBCursor MainCur =  RetCollection.find(searchQuery);
			DBObject MainObject = MainCur.next();
			if(MainObject.get("playerloginpassword")==null){
				return null;
			}
			if(((String)MainObject.get("playerloginpassword")).equals(HashedPassword)){
				return new Long((Integer)MainObject.get("plid"));
			}
			else {
				 return null;
				}
		}
		catch(java.lang.RuntimeException e){ //> making Static "long"
			return null;
		}
		catch(Exception e){
			e.printStackTrace();
			return (Long) null;
		}
	}
	public Block DownloadBlock(int blockid){ //Kiss keep it simple and stupid!
		MongoClient BlockMongoHandle=null;//Little Hack To Make Eclipse not Wine "uninitialized"
		try {
			BlockMongoHandle = new MongoClient(Constants.DBCBlockIP, Constants.DBCBlockPort);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DB BlockMongoDBHandle = BlockMongoHandle.getDB(Constants.DBCBlockDatabaseName);
		DBCollection RetCollection = BlockMongoDBHandle.getCollection(Constants.DBCBlockCollectionName);
		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put("blockid", blockid);
		DBCursor MainCur =  RetCollection.find(searchQuery);
		DBObject MainObject = MainCur.next();
		return new Block((new Float((Double)MainObject.get("block_x"))),(new Float((Double)MainObject.get("block_y"))),
				((Integer)MainObject.get("block_owner")), ((Integer)MainObject.get("guild_owner")));
	}//Note block "dbs" are designed to run locally on the realm
	public long GetTotalNumOfBlocks(){ //Kiss keep it simple and stupid!
		MongoClient BlockMongoHandle=null;//Little Hack To Make Eclipse not Wine "uninitialized"
		try {
			BlockMongoHandle = new MongoClient(Constants.DBCBlockIP, Constants.DBCBlockPort);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DB BlockMongoDBHandle = BlockMongoHandle.getDB(Constants.DBCBlockDatabaseName);
		DBCollection RetCollection = BlockMongoDBHandle.getCollection(Constants.DBCBlockCollectionName);
		return RetCollection.getCount();
	}
	public void SavePlayerData(PlayerData PlHandle){
		
	}
}
