package com.chill0n.mmotester.net.packet;

import com.chill0n.mmotester.net.Packet;

public interface BasePacket {
	public Packet getPacket();
}
