package com.chill0n.mmotester.net.packet;

import java.nio.ByteBuffer;

import com.chill0n.mmotester.net.Packet;
import com.chill0n.mmotester.net.PacketDescriptor;

public class ControlPacket {
	
	public final int PackCode = 6; //DESC Opcode!
	public final int Descriptor = 1, PPayloadSize;
	public final byte PControlCode;
	private Packet ReturnPacket;
	public ControlPacket(byte ControlCode, int PayloadSize){ //0==Begin 1===Done
		 ReturnPacket = new Packet(PackCode, new PacketDescriptor().setPacketDesc(Descriptor),
				 (short)1, ((ByteBuffer.allocate(5).put(ControlCode).putInt(PayloadSize)).array()));
		 PControlCode = ControlCode;
		 PPayloadSize = PayloadSize;
	}
	public Packet getPacket(){ //Standard
		return ReturnPacket;
	}
}
