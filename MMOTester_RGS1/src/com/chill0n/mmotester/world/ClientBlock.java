package com.chill0n.mmotester.world;

public class ClientBlock {
	public final byte ModelData[];
	public final byte TextureData[];
	public ClientBlock(byte modelData[], byte textureData[]){
		ModelData = modelData;
		TextureData = textureData;
	}
	@Deprecated // Only If Implemented In C++ Delete()
	public void AttemptCleanBlock(){
		System.gc();
	}
}
