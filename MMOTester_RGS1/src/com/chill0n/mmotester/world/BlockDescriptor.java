package com.chill0n.mmotester.world;

public class BlockDescriptor {
	public int PullDescriptorMap(int DescriptorHeader1, int DescriptorHeader2
			, int DescriptorHeader3, long offset){
		int returnOffset = (DescriptorHeader1>>2)*48;
		returnOffset += (DescriptorHeader2>>4)*48;
		
		return returnOffset;
	}
}
