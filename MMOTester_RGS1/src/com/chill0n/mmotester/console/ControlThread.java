package com.chill0n.mmotester.console;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Logger;

import com.chill0n.mmotester.constants.Constants;
import com.chill0n.mmotester.dbc.DBCController;
import com.chill0n.mmotester.player.PlayerData;
import com.chill0n.mmotester.player.SmallPlayerClass;
import com.chill0n.mmotester.shmem.SharedResManager;

public class ControlThread implements Runnable{
	private Socket initSocket;
	private OutputStream ooStream;
	private InputStream iiStream;
	private BufferedReader ConsoleIn;
	private PrintWriter ConsoleOut;
	private String R_password, R_username;
	private SimpleDateFormat df;
	protected final String CommandList[] = {
			"getuserdata - gets user data based on plid number from database", "help! - get here!"
			, "about?- gets info about this application"
	};
	public void InitSock(String usrname, String passwd, Socket srvSocket){
		initSocket = srvSocket;
		R_password = passwd;
		R_username = usrname;
	}
	@Override
	public void run() {
		try {
			df = new SimpleDateFormat("HH:mm");
			ooStream = initSocket.getOutputStream();
			iiStream = initSocket.getInputStream();
			ConsoleIn = new BufferedReader(new InputStreamReader(iiStream));
			ConsoleOut = new PrintWriter(ooStream);
			if(Constants.usePassword==true){
				SrvPrint("[Constants.Password]=on, Password, Username Required Random Set! " +
						"[NOUseDBForPasswd]=on");
				ConsoleOut.print("Console Username:");
				ConsoleOut.flush();
				String username = ConsoleIn.readLine();
				ConsoleOut.print("Console Password:");
				ConsoleOut.flush();
				String password = ConsoleIn.readLine();
				if(username.equals(R_username) && password.equals(R_password)){
					SrvPrintNoNewline("Sucessful Login!, Welcome\n");
				}
				else{
					SrvPrintNoNewline("Unsucessful Login!, Closing Stream [utf8]");
					ooStream.close();
					iiStream.close();
					initSocket.close();
				}
			}
			ConsoleOut.println("Logged Into Control Console (MMOTester). \n" +
					"'Work In Progress' So Be Patient!");
			ConsoleOut.flush();
			while(initSocket.isBound()){
				ConsoleOut.print("?>");
				ConsoleOut.flush();
				Poll(ConsoleIn.readLine());
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void Poll(String UTFString){
		if(UTFString.equals("getuserdata")){
			SrvPrint("Please Enter PLID For Player Lookup!:");
			try {
				DBCController DBCInstance = new DBCController();
				PlayerData PLData = DBCInstance.GatherLogin(Integer.parseInt(ConsoleIn.readLine())
						);
				SrvPrint("(1) Record Found!");
				SrvPrint("Player Name:"+PLData.PlayerName);
				SrvPrint("X Coordinate:"+PLData.CurCoordinates.Get_X_Coord()+" ,Y Coordinate:"+PLData.CurCoordinates.Get_Y_Coord()+
						", Z Coordinate:"+PLData.CurCoordinates.Get_Z_Coord());
				SrvPrint("[X Delta:"+PLData.CurCoordinates.Get_X_Delta()+"],[Y Delta:"+PLData.CurCoordinates.Get_Y_Delta()+"]"+
						"[Z Delta:"+PLData.CurCoordinates.Get_Z_Delta()+"]");
				for(int i=0;i<PLData.OwnedBlocks.size();i++){
					SrvPrint("Owns Block("+i+"):"+PLData.OwnedBlocks.get(i));
				}
				for(int i=0;i<PLData.OwnedBlocks.size();i++){
					SrvPrint("Owns Item("+i+"):"+PLData.OwnedItems.get(i));
				}
				SrvPrint("Player Level:"+PLData.PlayerLevel);
				SrvPrint("Player Class:"+PLData.PlayerClass);
				SrvPrint("Player TCP Port(*UNUSED*):"+PLData.PlayerTCPPort);
				SrvPrint("Player UDP Port(*UNUSED*):"+PLData.PlayerUDPPort);
				SrvPrint("Player Current Money:"+PLData.CurrentMoney);
				SrvPrint("Player GUILD:"+PLData.PlayerGuild);
				SrvPrint("Player State:"+PLData.PlayerState);
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				SrvPrint("[Exception]!"+e.getMessage());
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				SrvPrint("[Exception]!"+e.getMessage());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				SrvPrint("[Exception]!"+e.getMessage());
			}
		}
		/*else if(UTFString.equals("userlist")){
			SrvPrint("[User List]:");
				try{
				ArrayList<SmallPlayerClass> SMC = SharedResManager.GetSmallPlayers();
				SrvPrint("Players Found!:"+SMC.size());
				for(int i=0;i<SMC.size();i++){
					SrvPrint("PLID:["+SMC.get(i).plid+"]"+", Player Name:["+SMC.get(i).username);
				}
				} catch(NullPointerException e){
					SrvPrint("[Cancelled, This Command Execution Most Likely Due To The PlayerDataMap Not Being" +
							" Initalized So We Have Called An Interrupt]");
				}
		}*/
		else if(UTFString.equals("about?") || UTFString.equals("about")){
			SrvPrint("(MMOTester) Console! Version 1.0!, Written Soley By Noah Whiteis");
			SrvPrint("This Uses Raw TCP Communication With RCF-854 Telnet Protocol, Ive Implemented Everything But 39/m");
			SrvPrint("Gameserver Version:"+Constants.VVersion+", Gameserver Build:"+Constants.BuildNum);
			SrvPrint("[Warning]This Thread Is Strapped Isolated So If The Main Thread Fails This Thread Will Continue!");
		}
		else if(UTFString.equals("help!") || UTFString.equals("help")){
			for(int i=0;i<CommandList.length;i++){
				SrvPrint(CommandList[i]);
			}
		}
		else if(UTFString.equals("testhotspot")){
			SrvPrint("Hello!");
		}
		else if(UTFString.equals("testhotspota")){
			SrvPrint("Dynamic Class");
		}
		else{
			SrvPrint("(MMOConsole)Invalid Command!, Enter Help! For Commands");
		}
	}
	public void SrvPrint(String id){
		Calendar now = Calendar.getInstance();
		now.add(Calendar.MINUTE, 30);
		ConsoleOut.println("["+df.format(now.getTime())+"]"+">"+id);
		ConsoleOut.flush();
	}
	public void SrvPrintNoNewline(String id){
		Calendar now = Calendar.getInstance();
		now.add(Calendar.MINUTE, 30);
		ConsoleOut.print("["+df.format(now.getTime())+"]"+">"+id);
		ConsoleOut.flush();
	}
}
