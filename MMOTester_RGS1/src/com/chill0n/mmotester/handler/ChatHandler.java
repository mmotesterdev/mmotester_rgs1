package com.chill0n.mmotester.handler;

import java.nio.ByteBuffer;

import com.chill0n.mmotester.chat.ChatData;
import com.chill0n.mmotester.net.NetManager;
import com.chill0n.mmotester.net.Packet;
import com.chill0n.mmotester.shmem.SharedResManager;
import com.chill0n.mmotester.thread.PlayerThread;

public class ChatHandler {
	public ChatHandler(NetManager AccessGrid){
		//UNUSED 
	}
	public void HandleChat(Packet HandlePacket, PlayerThread ThreadHandle){
		ChatData ChatHandle = new ChatData();
		ByteBuffer ExtractorIndexOne = ByteBuffer.allocate(16);
		ExtractorIndexOne.put(HandlePacket.getPacketData());
		long m_plid;
		int tag_size, msg_size;
		ExtractorIndexOne.clear(); //Set Pointer To Zero
		m_plid = ExtractorIndexOne.getLong();
		tag_size = ExtractorIndexOne.getInt(); //Get Tag Size
		msg_size = ExtractorIndexOne.getInt(); //Get Message Size
		byte tag_out[] = new byte[tag_size]; //Allocate To Size
		byte msg_out[] = new byte[msg_size]; //Allocate
		for(int i=0;i<=tag_size;i++){
			tag_out[i] = ExtractorIndexOne.get(i); //Get Message
		}
		for(int i=0;i<=msg_size;i++){
			msg_out[i] = ExtractorIndexOne.get(i);
		}
		ChatHandle.tag = new String(tag_out);
		ChatHandle.data = new String(msg_out);
		ChatHandle.plid = m_plid;
		ThreadHandle.CurrentSharedBlock.PushChatUpdate(ChatHandle); //Ok Lets Cache And Move On
	}
}
