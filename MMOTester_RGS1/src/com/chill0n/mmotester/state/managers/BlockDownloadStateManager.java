package com.chill0n.mmotester.state.managers;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import com.chill0n.mmotester.net.Packet;
import com.chill0n.mmotester.net.packet.BlockDownloadPacket;
import com.chill0n.mmotester.nparchive.NPAReader;
import com.chill0n.mmotester.player.PlayerData;
import com.chill0n.mmotester.world.ClientBlock;

public class BlockDownloadStateManager {
	PlayerData PlayerHandle;
	boolean needsDownloadedBlock, hasDownloadedBlock;
	public BlockDownloadStateManager(PlayerData plobj){
		PlayerHandle = plobj;
		needsDownloadedBlock=true;
		hasDownloadedBlock=false;
	}
	public ArrayList<Packet> CheckDownloadState(){
		if(needsDownloadedBlock==true){
			NPAReader NPAInstance = new NPAReader(null,(Integer) null);
			ClientBlock NBlock = NPAInstance.ParseNPAClient(PlayerHandle.CurBlock);
			BlockDownloadPacket BlockDownloader = null;
			try {
				BlockDownloader = new BlockDownloadPacket(PlayerHandle.CurBlock,
						NBlock.ModelData, NBlock.TextureData);
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Packet InitPacket = BlockDownloader.GetDownloadPacket();
			ArrayList<Packet> ModelPackets = BlockDownloader.getModelPackets();
			ArrayList<Packet> TexturePackets = BlockDownloader.getTexturePackets();
			ArrayList<Packet> FinalPacket = new ArrayList<Packet>();
			FinalPacket.add(InitPacket);
			FinalPacket.addAll(ModelPackets);
			FinalPacket.addAll(TexturePackets);
			hasDownloadedBlock=true;
			return FinalPacket;
		}
		else{
			hasDownloadedBlock=false;
			return null;
		}
	}
}
