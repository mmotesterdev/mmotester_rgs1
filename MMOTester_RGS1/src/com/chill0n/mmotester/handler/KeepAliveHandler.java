package com.chill0n.mmotester.handler;

import java.io.IOException;

import com.chill0n.mmotester.net.NetManager;
import com.chill0n.mmotester.net.Packet;

public class KeepAliveHandler { //MAIN Indicated The Method To Be Called
	long KAHCount=0;
	long KAHCountRequired = 50;
	NetManager MainNetMGR;
	public KeepAliveHandler(NetManager AccessGrid){
		MainNetMGR = AccessGrid;
	}
	public void KeepAliveMAIN(Packet KeepAlivePacket){ //Just Do Nothing This Is Only TO Keep TCP Alive
		KAHCount = KAHCount+1; //Only Check On Every 50 Count!
		if(KAHCount==KAHCountRequired){
			KAHCountRequired=0;
			if(KeepAlivePacket==null){
				try {
					MainNetMGR.CleanupSockets();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				MainNetMGR.pthread.ExitThread();
			}
		}
		else{
			return;
		}
	}
}
