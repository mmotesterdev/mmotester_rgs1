package com.chill0n.mmotester.console;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import com.chill0n.mmotester.constants.Constants;

public class MMOConsoleThread implements Runnable{
	@Override
	public void run() {
		System.out.println("[INFO]Console Thread Startup Sucessful!");
		System.out.println("[INFO]Console Thread Using [Constant] Username="+Constants.ConsoleUsername+" Password="+
		Constants.ConsolePassword + " On Port:["+Constants.DebugConsolePort+"]");
		ServerSocket SrvSocket=null;
		try {
			SrvSocket = new ServerSocket(Constants.DebugConsolePort);
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
			Thread.currentThread().interrupt();
		}
		ControlThread ControlInstance = new ControlThread();
		while(Thread.currentThread().isInterrupted()!=true){
			try {
				ControlInstance.InitSock(Constants.ConsoleUsername, Constants.ConsolePassword, 
						SrvSocket.accept());
				new Thread(ControlInstance).start();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				try {
					SrvSocket.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				e.printStackTrace();
			}
		}
	}

}
