# README #

MMOTester Server RGS1, A Complementary Dummy Client Is Available As MMOTester_SR1

### MMOTester Server Builds ###

* 
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* MongoDB Is Required For Deployment, Reference Database Available
* Include MongoDB Java Driver In Classpath
* Default Setting Is For The Database Connection To Bind To The Loopback Device
* This Is Tunnable In Constants In The DBCUrl
* An Eclipse Config Is Included In The Repo

### Managment Console ###

* The Default Production Management Authority, Is Through The SSCAT (Secure Server Control Authority Tunnel)
* A Small Development Console Was Implemented Last Time I Worked On MMOTester
* DO Not Use This Over The Internet, (Unless Properly Tunneled), Since Uses The Telnet Protocol, Which Is Inherently Unsecure
* Use Telnet [Loopback Default]:30293  

### More ###

* Not Very Much Has Been Worked On, But This Is A Message :)