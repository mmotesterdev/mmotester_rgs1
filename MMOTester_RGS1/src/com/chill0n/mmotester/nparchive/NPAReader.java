package com.chill0n.mmotester.nparchive;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.ArrayList;

import com.chill0n.mmotester.nparchive.json.JsonArray;
/*
 * NPAS Are Either Two Things 1.A Compressed (Or Uncompressed) JSON File 2.A Archive Used To Map Models
 * And Different Static Empl
 */
import com.chill0n.mmotester.nparchive.json.JsonObject;
import com.chill0n.mmotester.world.Block;
import com.chill0n.mmotester.world.ClientBlock;
import com.chill0n.mmotester.world.GameAsset;
public class NPAReader {
public static final int NPA_BLOCK_NET = 0;//Network Block Descriptor
	public static final int NPA_BLOCK_CLI = 1;//Client Block
	public static final int MAP_CLI = 2;
	private String FileName;
	private int FNPAType;
	public NPAReader(String fname, int NPAType){
		FileName = fname;
		FNPAType = NPAType;
	}
	public Block ParseNPA(){
		/*
		ArrayList<GameAsset> GameAssetList = new ArrayList<GameAsset>();
		JsonObject jsonMainNPA = JsonObject.readFrom(new FileReader(FileName));
		jsonMainNPA = 
		//TODO RIGHT AWAY MUST IMPLEMENT
		Block TempBlock = new Block(jsonMainNPA.get("XCoord").asFloat(),jsonMainNPA.get("YCoord").asFloat(),
				,jsonMainNPA.get("NacpackFile").asArray().values());
		*/
		return null;
	}
	public ClientBlock ParseNPAClient(int blockid){
		File ReadBlockModel = new File("blockmodels/"+Integer.toString(blockid));
		File ReadBlockTexture = new File("blocktextures/"+Integer.toString(blockid));
		byte ModelData[] = new byte[(int)ReadBlockModel.length()];
		byte TextureData[] = new byte[(int)ReadBlockTexture.length()];
		if(ReadBlockModel.exists()!=true || ReadBlockTexture.exists()!=true){
			//TODO Handle File "Existence" Problems
		}
		try {
			DataInputStream TextureDS = new DataInputStream(new FileInputStream(ReadBlockTexture));
			DataInputStream ModelDS = new DataInputStream(new FileInputStream(ReadBlockModel));
			TextureDS.readFully(TextureData);
			ModelDS.readFully(ModelData);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch(IOException e){
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ClientBlock returnBlock = new ClientBlock(ModelData, TextureData);
		return returnBlock;//TODO URGENT
	}
	
}
