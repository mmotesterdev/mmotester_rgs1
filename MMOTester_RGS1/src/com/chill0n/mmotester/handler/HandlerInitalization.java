package com.chill0n.mmotester.handler;

import com.chill0n.mmotester.net.NetManager;

public class HandlerInitalization {
	private NetManager AccessGrid;
	/*
	 * This Class Is Used TO Declare The Handler Objectes Reference
	 * Just Insert Packet Object And Handler Object Then Add Object TO 
	 * Packet Handler For Recognized Opcode And Walla You Have A Packet
	 */
	public KeepAliveHandler KeepAliveHandle;
	public LoginHandler LoginHandle;
	public ChatHandler ChatHandle;
	public DeltaUpdateHandler DeltaUpdateHandle;
	public HandlerInitalization(NetManager AccessHandle){
		AccessGrid = AccessHandle;
		KeepAliveHandle = new KeepAliveHandler(AccessGrid); //0x1
		LoginHandle = new LoginHandler(AccessGrid); //0x2
		ChatHandle = new ChatHandler(AccessGrid); //0x3
		DeltaUpdateHandle = new DeltaUpdateHandler(AccessGrid); //0x4
	}
}
