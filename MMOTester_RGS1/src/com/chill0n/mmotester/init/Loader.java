package com.chill0n.mmotester.init;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.management.ManagementFactory;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.logging.Logger;

import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import com.chill0n.mmotester.console.MMOConsoleThread;
import com.chill0n.mmotester.constants.Constants;
import com.chill0n.mmotester.dbc.DBCController;
import com.chill0n.mmotester.io.DebugIO;
import com.chill0n.mmotester.shmem.SharedResManager;
import com.chill0n.mmotester.shmem.SharedResManager2;
import com.mongodb.util.Base64Codec;

public class Loader {
	public static void main(String args[]){
		DebugIO.PrintOutStandard("MMOTester Server DEBUG"+Constants.VVersion);
		DebugIO.PrintOutStandard("Build#:"+Constants.BuildNum+", Lead Maintainer:"+Constants.BuildEmp);
		DebugIO.PrintOutStandard(Constants.Copyright);
		DebugIO.PrintOutStandard("Bootstrap Sucessful! Loading Of DYLIB Using JNI");
		DebugIO.PrintOutStandard("JVM Probe Results:");
		DebugIO.PrintOutStandard("JVM Version:"+ManagementFactory.getRuntimeMXBean().getVmVersion());
		DebugIO.PrintOutStandard("Available Cores#:"+Runtime.getRuntime().availableProcessors());
		DebugIO.PrintOutStandard("Free Memory:"+(Runtime.getRuntime().freeMemory()));
		DebugIO.PrintOutStandard("Maximum Memory:"+Runtime.getRuntime().maxMemory());
		DebugIO.PrintOutStandard("Total Memory:"+Runtime.getRuntime().totalMemory());
		DebugIO.PrintOutStandard("OK!, Starting Build In "+Constants.isDebugString+ " MODE");
		DebugIO.PrintOutStandard("========================================================");
		SharedResManager2.SynchronizeMemoryMap_USEONLYONCE();
		SharedResManager2.BuildBlockMemoryMap_USEONLYONCE();
		InitGSocket iGs = new InitGSocket();
		ServerSocket InitalizationSock;
		try {
			InitalizationSock = new ServerSocket();
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		SSLSocketFactory SSLIndustries = (SSLSocketFactory) SSLSocketFactory.getDefault();
		InitalizationSock = null;
		try {
			InitalizationSock = new ServerSocket(Constants.TCPPort);
			SSLIndustries = (SSLSocketFactory) SSLSocketFactory.getDefault();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		new Thread(new MMOConsoleThread()).start(); //START CONSOLE THREAD
		int CloseCounter = 0;
		if(Constants.isDebugMode){
			for(int i=0;i<Constants.DebugThreadPoolSize;i++){
				DebugIO.PrintOutStandard("[THLOADER]Registering Debug Thread#:"+i+", PLID#:"+Constants.DebugAcceptablePLID[i]);
				//iGs.RegisterThread(1, Constants.DebugAcceptablePLID[i], Constants.DebugRadomKey);
			}//TODO Reimplement Debug Mode
		}
		else{while(true){
			try {
				if(CloseCounter==0){
					System.out.println("[INFO] TCP Listening On "+Constants.TCPPort);
				}else{
					System.out.println("[INFO](Recovered!) TCP Listening On "+Constants.TCPPort);
				}
				CloseCounter++;
				while(Thread.currentThread().isInterrupted()!=true){
					Socket StandardInitSock = InitalizationSock.accept();
					iGs.RegisterThread(StandardInitSock);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		}
	}
	
}
