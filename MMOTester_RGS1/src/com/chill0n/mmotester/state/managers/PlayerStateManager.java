/**
 * 
 */
package com.chill0n.mmotester.state.managers;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

import com.chill0n.mmotester.net.Packet;
import com.chill0n.mmotester.net.packet.PlayerUpdatePacket;
import com.chill0n.mmotester.player.PlayerData;
import com.chill0n.mmotester.shmem.SharedResManager;
import com.chill0n.mmotester.thread.PlayerThread;

/**
 * @author nwhiteis
 *
 */
public class PlayerStateManager {
	Queue<Packet> WaitingForUpdate;
	public PlayerStateManager(){
		WaitingForUpdate = new LinkedList<Packet>();
		//TODO Replace Queue With Live Round Robin *URGENT*, So No 1-Update At A Time Mentality
	}
	public ArrayList<Packet> CheckPlayer(PlayerThread ThreadHandle){
		ArrayList<Packet> RetPacket = new ArrayList<Packet>();
			ArrayList<PlayerData> PacketRaw = ThreadHandle.CurrentSharedBlock.
					CheckAndOrPullPlayerUpdate(ThreadHandle.PlayerInfo.PlayerID);
			for(int i=0;i<PacketRaw.size();i++){
				RetPacket.add(new PlayerUpdatePacket(PacketRaw.get(i)).getPacket());
			}
			return RetPacket;
	}
	public int CheckWaits(){
		return WaitingForUpdate.size();
	}
	public Packet getPrematurePacket(){
		return WaitingForUpdate.remove();
	}
}
