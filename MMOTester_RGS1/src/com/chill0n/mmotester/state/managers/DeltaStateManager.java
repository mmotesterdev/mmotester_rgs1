package com.chill0n.mmotester.state.managers;

import java.util.ArrayList;

import com.chill0n.mmotester.net.Packet;
import com.chill0n.mmotester.net.packet.DeltaUpdatePacket;
import com.chill0n.mmotester.player.PlayerCoordinate;
import com.chill0n.mmotester.shmem.SharedResManager;
import com.chill0n.mmotester.thread.PlayerThread;

public class DeltaStateManager {
	public ArrayList<Packet> CheckDelta(PlayerThread ThreadHandle){
		ArrayList<Packet> returnPackets = new ArrayList<Packet>();
		ArrayList<PlayerCoordinate> CacheDeltas = new ArrayList<PlayerCoordinate>();
		returnPackets.add(new DeltaUpdatePacket(CacheDeltas).getPacket());
		return returnPackets;
	}
}
