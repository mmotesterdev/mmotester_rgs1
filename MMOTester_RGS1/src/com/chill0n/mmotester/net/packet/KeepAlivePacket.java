package com.chill0n.mmotester.net.packet;

import com.chill0n.mmotester.net.Packet;
import com.chill0n.mmotester.net.PacketDescriptor;

public class KeepAlivePacket implements BasePacket{
	/*
	 * "Keep Alive" is intended to keep booth udp and tcp connections
	 * "alive", this packet is more important to udp since udp connections
	 * require the NAT to keep the same port mappings, tcp does not suffer
	 * this same problem in the usual sense since tcp is a state based protocol
	 * and uses a stream to keep the connection alive, although termination may
	 * occour so tcp versions are sent less ofsetn
	 */
	public final int PackCode = 0; //Keep Alive Please!!!
	public final int Descriptor = 3; //Booth UDP or TCP
	Packet ReturnPack;
	public KeepAlivePacket(){
		byte PacketData[] = {
				0x1a, 0x1a, 0x1a, 0x1a
		};//4 Bytes Wide
		ReturnPack = new Packet(PackCode, new PacketDescriptor().setPacketDesc(Descriptor), (byte)4, PacketData);
	}
	public Packet getPacket() {
		return ReturnPack;
	}
}
