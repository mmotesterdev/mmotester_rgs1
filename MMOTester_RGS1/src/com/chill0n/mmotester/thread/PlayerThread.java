package com.chill0n.mmotester.thread;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.ArrayList;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import com.chill0n.mmotester.chat.ChatData;
import com.chill0n.mmotester.constants.Constants;
import com.chill0n.mmotester.dbc.DBCController;
import com.chill0n.mmotester.net.NetManager;
import com.chill0n.mmotester.net.Packet;
import com.chill0n.mmotester.net.PacketHandler;
import com.chill0n.mmotester.net.UDPPacket;
import com.chill0n.mmotester.net.packet.ControlPacket;
import com.chill0n.mmotester.player.PlayerData;
import com.chill0n.mmotester.shmem.SharedResManager;
import com.chill0n.mmotester.shmem.SharedResManager2;
import com.chill0n.mmotester.state.StateManager;
import com.chill0n.mmotester.world.Block;

public class PlayerThread implements Runnable{
	public Block CurrentSharedBlock;
	public SharedResManager2 SharedCurrency;
	public PlayerData PlayerInfo;
	protected int UDPPort;
	public NetManager NetMGR;
	public PacketHandler PacketHandle;
	protected StateManager StateChecker;
	private int StateReturnTableSize;
	private long plid;
	private ThreadDescriptor pldesc;
	public PlayerThread(ThreadDescriptor PRIVATE_pldesc){
		pldesc = PRIVATE_pldesc;
	}
	@Override
	public void run() { //Java Specific f88 thread
		//(int Port, int UDPPort, int AssignedPort, byte RandomKey[], long plid, PlayerThread thread)
				Login(pldesc.TCPSock);
				NetMGR = new NetManager(pldesc.TCPSock, plid, this);
				PacketHandle = NetMGR.getHandleClass();
				SharedCurrency = new SharedResManager2();
				StateChecker = new StateManager(this);
				StateReturnTableSize = StateChecker.getReturnSize();
				try {
					PlayerInfo = new DBCController().GatherLogin(plid);
					CurrentSharedBlock = SharedResManager2.GetBlock(PlayerInfo.PlayerID);
					CurrentSharedBlock.joinBlock(PlayerInfo);
				} catch (UnknownHostException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				//Game Loop, (0)=Open (1)=Closed
				ArrayList<ArrayList<Packet>> SendQueue; //*Unloaded*
				while(Thread.interrupted()!=true){ // No Interrupts Fired
					//*BEGIN*7 Debug Interrupts
					try{
					int PayLoadIncrement=0;
					/*Interrupt******/
					System.out.println("Systems Interrupt 01");
					/*Interrupt******/
					SendQueue = StateChecker.ProcessVector();
					for(int i=0;SendQueue.size()>i;i++){
						for(int b=0;b<SendQueue.get(i).size();i++){
							PayLoadIncrement++;
							/*Interrupt******/
							System.out.println("Systems Interrupt 02");
							/*Interrupt******/
						} //Get Size? 
					}
					/*Interrupt******/
					System.out.println("Systems Interrupt 03");
					/*Interrupt******/
					NetMGR.SendTCP(new ControlPacket((byte)0,PayLoadIncrement).getPacket());
					/*Interrupt******/
					System.out.println("Systems Interrupt 04");
					/*Interrupt******/
					for(int i=0;SendQueue.size()>i;i++){
						for(int b=0;b<SendQueue.get(i).size();i++){
							/*Interrupt******/
							System.out.println("Systems Interrupt 05");
							/*Interrupt******/
							NetMGR.SendTCP(SendQueue.get(i).get(b));
							/*Interrupt******/
							System.out.println("Systems Interrupt 06");
							/*Interrupt******/
						}
					}
					/*Interrupt******/
					System.out.println("Systems Interrupt 07");
					/*Interrupt******/
					ArrayList<Packet>ReceivedPacket = new ArrayList<Packet>();
					NetMGR.SendTCP(new ControlPacket((byte)1,0).getPacket());
					Packet ControlState = NetMGR.pollTCP();
					ControlPacket Finder = DeserializeCPacket(ControlState);
					for(int i=0;i<Finder.PPayloadSize;i++){
						ReceivedPacket.add(NetMGR.pollTCP());
					}
					/*Interrupt******/
					System.out.println("Systems Interrupt 08");
					/*Interrupt******/
				} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
				//END
	}
	public void Login(Socket StandardInitSock){
		try{
		SSLSocketFactory SSLIndustries = (SSLSocketFactory) SSLSocketFactory.getDefault();
		SSLSocket LoginSock = (SSLSocket) SSLIndustries.createSocket(StandardInitSock, null, StandardInitSock.getPort(), 
				false);
		LoginSock.setUseClientMode(false);
		InputStream LoginInput = LoginSock.getInputStream();
		OutputStream LoginOutput = LoginSock.getOutputStream();
		DBCController CheckController = new DBCController();
		ByteBuffer returnBuffer = ByteBuffer.allocate(52);
		byte insetFull[] = new byte[52];
		LoginInput.read(insetFull);
		if(insetFull.length!=52){
			//Not 52 Bytes Terminate!
			LoginOutput.write("0x01:STROUT-Malformed Buffer Is Too Small !=52".getBytes());
			LoginInput.close(); LoginOutput.close(); LoginSock.close(); //Close The SStream Sockets
			StandardInitSock.close();
		}
		returnBuffer.put(insetFull);
		returnBuffer.clear();
		int MagicNumber; long BuildNum;
		byte UserHash[] = new byte[20];
		byte PasswordAlreadyHash[] = new byte[20];
		MagicNumber = returnBuffer.getInt();
		BuildNum = returnBuffer.getLong();
		returnBuffer.get(UserHash, 0, 20);
		returnBuffer.get(PasswordAlreadyHash, 0, 20);
		LoginOutput.write(Constants.LoginACK.getBytes());
		Long Long_plid = CheckController.CheckLogin(GetSHA1HexFromBytes(PasswordAlreadyHash), GetSHA1HexFromBytes(UserHash));
		if(Long_plid!=null && MagicNumber==0xface && BuildNum==Constants.BuildNumInt){
			LoginOutput.write(Constants.LoginGood.getBytes());
			LoginInput.close(); LoginOutput.close(); LoginSock.close(); //Close The SStream Sockets
			plid = Long_plid;
		}
		else{
			LoginOutput.write(Constants.LoginBad.getBytes());
			LoginInput.close(); LoginOutput.close(); LoginSock.close(); //Close The SStream Sockets
			StandardInitSock.close();
			Thread.currentThread().interrupt();
		}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public void ExitThread(){
		Thread.currentThread().interrupt();
	}
	public static String GetSHA1HexFromBytes(byte Input[]){
		StringBuffer TempBuffer = new StringBuffer();
	    for(int i=0;i<Input.length;i++){
	    	TempBuffer.append(Integer.toString((Input[i] & 0xFF) + 0x100, 16));
	    }
	    return TempBuffer.toString();
	}
	public ControlPacket DeserializeCPacket(Packet PacketRawPack){
		if(PacketRawPack.getPacketData().length>5){
			return null; //Malformed
		}
		else{
			ByteBuffer RetBuff = ByteBuffer.allocate(5).put(PacketRawPack.getPacketData());
			RetBuff.clear();
			int PackSize = RetBuff.getInt();
			byte PackType = RetBuff.get();
			return new ControlPacket(PackType,PackSize);
		}
	}
}
