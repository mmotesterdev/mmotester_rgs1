package com.chill0n.mmotester.player;

import java.util.ArrayList;

public class GMPlayerData extends PlayerData{
	private String GMEmail;
	private String GMRealName;
	private int GMLevel;
	public GMPlayerData(long plid, PlayerCoordinate curcoordinate, int curblock, String playername, int playerlevel,
			int playerclass, int playertcpport, int playerudpport, byte plraneomkey[], ArrayList<Long> ownedblocks,
			double currentmoney, ArrayList<Integer>owneditems, long playerguild, int playerstate) {
		super(plid, curcoordinate, curblock, playername, playerlevel, playerclass, playertcpport, playerudpport, 
				 ownedblocks, currentmoney, owneditems, playerguild, playerstate);
	}
	public String getEmail(){
		return GMEmail;
	}
	public String getRealName(){
		return GMRealName;
	}
	public int getGMLevel(){
		return GMLevel;
	}
}
