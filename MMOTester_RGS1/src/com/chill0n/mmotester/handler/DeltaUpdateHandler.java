package com.chill0n.mmotester.handler;

import java.nio.ByteBuffer;

import com.chill0n.mmotester.net.NetManager;
import com.chill0n.mmotester.net.Packet;
import com.chill0n.mmotester.player.PlayerCoordinate;
import com.chill0n.mmotester.shmem.SharedResManager;
import com.chill0n.mmotester.thread.PlayerThread;

public class DeltaUpdateHandler {
	public DeltaUpdateHandler(NetManager AccessGrid){
		
	}
	public void DeltaUpdateMAIN(Packet HandledPacket, PlayerThread ThreadHandle){
		long plid, timestamp;
		float x_coord, y_coord, z_coord, x_delta, y_delta, z_delta;
		ByteBuffer MainBuffer = ByteBuffer.allocate(48);
		MainBuffer.put(HandledPacket.getPacketData());
		MainBuffer.clear(); //Set the buffers pointer to 0
		if(MainBuffer.getInt()>1){ //Handle This Problem Later, The Client Should Not supply multiple values
			//TODO Handle Exception Properly, What IS This Client Thinking
		}
		plid = MainBuffer.getLong();
		timestamp = MainBuffer.getLong();
		x_coord = MainBuffer.getFloat();
		y_coord = MainBuffer.getFloat();
		z_coord = MainBuffer.getFloat();
		x_delta = MainBuffer.getFloat();
		y_delta = MainBuffer.getFloat();
		z_delta = MainBuffer.getFloat();
		PlayerCoordinate DumpCoordinate = new PlayerCoordinate(plid, timestamp, x_coord, y_coord, z_coord, 
				x_delta, y_delta, z_delta);
		ThreadHandle.PlayerInfo.CurCoordinates = DumpCoordinate;
	}
}
