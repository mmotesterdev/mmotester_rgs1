package com.chill0n.mmotester.net;

import java.lang.reflect.Array;
import java.nio.ByteBuffer;
import java.util.Arrays;

public class Packet {
	protected int PacketHeader; //32
	protected int PacketType; //1 Or 2 See Below //32
	protected PacketDescriptor PacketDesc;
	protected short PacketDataSize; //16
	protected byte PacketData[]; //Unlimited
	private NetManager nmanagerhandle;
	public Packet(int Header, PacketDescriptor Desc ,short Datasize, byte Data[]){  //Type = 1
		//[32Bit][Header][16Bit][Datasize][8Bit]Data Array
		PacketHeader = Header;
		PacketDesc = Desc;
		PacketDataSize = Datasize;
		PacketData = Data;
		PacketType = 1;  //Type1 Of Course
	}
	public Packet(int Header, short Datasize, byte Data[]){ //Downsized Version Type = 2
		PacketHeader = Header;
		PacketDataSize = Datasize;
		PacketData = Data;
	}
	public Packet(int Header, short Datasize ,int Checksum, int Order, byte Data[]){ 
		//Future Implementation Prehaphs Replacing TCP
		
	}
	public int getPacketType(){
		return PacketType;
	}
	public int getDataSize(){
		return PacketDataSize;
	}
	public PacketDescriptor getPacketDesc(){
		return PacketDesc;
	}
	public byte[] getPacketData(){
		return PacketData;
	}
	public int getPacketHeader(){
		return PacketHeader;
	}
	public byte[] getPacketBytes(){
			byte RPacketHeader[] = ByteBuffer.allocate(4).putInt(PacketHeader).array();
			byte RPacketType[] = ByteBuffer.allocate(4).putInt(PacketType).array();
			byte RPacketSize[] = ByteBuffer.allocate(2).putShort(PacketDataSize).array();
			ByteBuffer ReturnArr = ByteBuffer.allocate(10+PacketDataSize);
			ReturnArr.put(0,RPacketHeader[0]);//  32 Bits(4 bytes) Header
			ReturnArr.put(1,RPacketHeader[1]);//    |
			ReturnArr.put(2,RPacketHeader[2]);//   _|_
			ReturnArr.put(3,RPacketHeader[3]);//   \|/
			ReturnArr.put(4,RPacketType[0]); // 32 Bits(4 bytes) Packet Type
			ReturnArr.put(5,RPacketType[1]);//      |
			ReturnArr.put(6,RPacketType[2]);//     _|_
			ReturnArr.put(7,RPacketType[3]);//     \|/
			ReturnArr.put(8,RPacketSize[0]);//  16 Bits(2 bytes)
			ReturnArr.put(9,RPacketSize[1]);//     \|/
			if(PacketType==1){ //Simple And Elegant Solution
				ReturnArr.putLong(nmanagerhandle.UDPCounter);//If UDP Put Counter Point For Ordering
			}
			{
				ReturnArr.put(PacketData);
			}
		return ReturnArr.array();
	}
	
}


