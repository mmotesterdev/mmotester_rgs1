package com.chill0n.mmotester.net.packet;

import java.nio.ByteBuffer;

import com.chill0n.mmotester.net.Packet;
import com.chill0n.mmotester.net.PacketDescriptor;

public class ChatPacket {
	public final int PackCode = 3;
	public final int Descriptor = 2;
	Packet ReturnPacket;
	public ChatPacket(long plid, String tag, String msg){
		ByteBuffer FinalData = ByteBuffer.allocate(tag.length()+msg.length());
		FinalData.putLong(plid); // Player ID 8 Bytes
		FinalData.putInt(tag.getBytes().length); //Size 4 Bytes
		FinalData.putInt(msg.getBytes().length); //Size 4 Bytes
		FinalData.put(tag.getBytes());
		FinalData.put(msg.getBytes()); //MSG Utf8 SSLM
		ReturnPacket = new Packet(PackCode, new PacketDescriptor().setPacketDesc(Descriptor)
				,(short)(tag.length()+msg.length()), FinalData.array());
	}
	public Packet getPacket() {
		return ReturnPacket;
	}

}
