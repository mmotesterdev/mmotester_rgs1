package com.chill0n.mmotester.net.packet;

import java.nio.ByteBuffer;

import com.chill0n.mmotester.net.Packet;
import com.chill0n.mmotester.net.PacketDescriptor;

public class LoginPacket{
	public final int PackCode = 1;
	public final int Descriptor = 2; //Same As Type!
	Packet ReturnPacket = null;
	private String PacketPUsername, PacketPPassword;
	public LoginPacket(String PUsername, String PPassword) {
		int Header, Desc;
		short Datasize;
		PacketPPassword = PPassword;
		PacketPUsername = PUsername;
		ByteBuffer TempSend = ByteBuffer.allocate(PUsername.getBytes().length+PPassword.getBytes().length);
		TempSend.put(PUsername.getBytes()); //Put Username 
		TempSend.put(PPassword.getBytes()); //Put Password
		byte ReturnBuffer[] = new byte[PUsername.getBytes().length+PPassword.getBytes().length];
		TempSend.get(ReturnBuffer);
		Packet LoginPacketRet = new Packet(PackCode, new PacketDescriptor().setPacketDesc(Descriptor)
				, (short)((PUsername.getBytes().length)+(PPassword.getBytes().length)),
				ReturnBuffer);
		ReturnPacket = LoginPacketRet;
	}
	public Packet getPacket(){ //Standard
		return ReturnPacket;
	}
	public String getPPassword(){
		return PacketPPassword;
	}
	public String getPUsername(){
		return PacketPUsername;
	}
}
