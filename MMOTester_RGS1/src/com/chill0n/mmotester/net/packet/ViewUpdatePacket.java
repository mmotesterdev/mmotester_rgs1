package com.chill0n.mmotester.net.packet;

import java.nio.ByteBuffer;
import java.util.ArrayList;

import com.chill0n.mmotester.net.Packet;
import com.chill0n.mmotester.net.PacketDescriptor;
import com.chill0n.mmotester.player.PlayerView;

public class ViewUpdatePacket {
	public final int PackCode = 7;
	public final int Descriptor = 1; //Same As Type!
	private Packet PacketRetPack;
	public ViewUpdatePacket(ArrayList<PlayerView> PlayerViewArr){
		ByteBuffer TempBuffer = ByteBuffer.allocate((PlayerViewArr.size()*(20))+4); //20 Bytes (long,8)+(float,4)+(float),4+(float,4)
		TempBuffer.putInt(PlayerViewArr.size()); //Size Only Once
		for(int i=0;i<=PlayerViewArr.size();i++){
        	TempBuffer.putLong(PlayerViewArr.get(i).getPLID()); //PLID [i]
        	TempBuffer.putFloat(PlayerViewArr.get(i).getYaw()); // YAW [i]
        	TempBuffer.putFloat(PlayerViewArr.get(i).getPitch()); // PITCH[i]
        	TempBuffer.putFloat(PlayerViewArr.get(i).getRoll()); // ROLL[i]
        }
		PacketRetPack = new Packet(PackCode,new PacketDescriptor().setPacketDesc(Descriptor),
				(short)(PlayerViewArr.size()*(16)+4),TempBuffer.array());
	}
	public Packet getPacket(){
		return PacketRetPack;
	}
}
