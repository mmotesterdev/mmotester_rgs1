package com.chill0n.mmotester.net.packet;

import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Random;

import com.chill0n.mmotester.net.Packet;
import com.chill0n.mmotester.net.PacketDescriptor;

public class BlockDownloadPacket {
	/*
	 * Encapsulates And Sends BlockDownLoadPackets, Also Generates Fragments
	 * Hash is Not for Fragment By Fragment Hash Is Instead Used To Mark The Whole Download
	 * Booth Models And Textures Use The Same DownloadID
	 * The DownloadID is Not Intermenapid To The SecureRandoms Thread So Collisons Are Unliklely
	 * Considering Only A thousand Players Even Though Is Is Far Less Random But Faster!
	 */
	public final int PackCode = 7;
	public final int Descriptor = 2; //TCP
	protected final int ModelBlockFragments, TextureBlockFragments;
	protected final long DownloadID;
	protected final byte TextureMD5Checksum[], ModelMD5Checksum[];
	protected Packet ReturnDownloadPacket;
	protected final int BlockID;
	protected ArrayList<ModelFragmentPacket> ModelList;
	protected ArrayList<TextureFragmentPacket> TextureList;
	//Initalized to the main byte[] which is sent
	public BlockDownloadPacket(int Blockid ,byte blockModelData[], byte blockTextureData[]) throws NoSuchAlgorithmException{
		Random DownloadIDGenerator = new Random();
		BlockID = Blockid;
		DownloadID = DownloadIDGenerator.nextLong(); //? Generate RandomID
		ModelMD5Checksum = MessageDigest.getInstance("MD5").digest(blockModelData);
		TextureMD5Checksum = MessageDigest.getInstance("MD5").digest(blockTextureData);
		byte ModelBuffer[] = new byte[20000]; //Get Checksum From AXOR*(B)
		byte TextureBuffer[] = new byte[20000];
		int BlockResetCounter=0, BlockCounter=1;
		for(int i : blockModelData){
			BlockResetCounter=BlockResetCounter+1;
			if(i==20001 && blockModelData.length>20000){//20Kb
				BlockResetCounter=0;
				BlockCounter=BlockCounter+1;
				ModelList.add(new ModelFragmentPacket(DownloadID ,BlockCounter,ModelBuffer));
				ModelBuffer = new byte[20000]; //Reallocate
			if(i==blockModelData.length && blockModelData.length==20000){
				ModelList.add(new ModelFragmentPacket(DownloadID ,BlockCounter,ModelBuffer));
			}
			}else{
			ModelBuffer[BlockResetCounter] = blockModelData[i];
			}
		}
		ModelBlockFragments = BlockCounter;
		int TextureResetCounter=0, TextureCounter=1;
		for(int i : blockTextureData){
			TextureResetCounter=TextureResetCounter+1;
			if(i==20001 && blockModelData.length>20000){//20Kb
				TextureResetCounter=0;
				TextureCounter=TextureCounter+1;
				TextureList.add(new TextureFragmentPacket(DownloadID, TextureCounter ,TextureBuffer));
				TextureBuffer = new byte[20000]; //Reallocate
			if(i==blockTextureData.length && blockTextureData.length==20000){
				TextureList.add(new TextureFragmentPacket(DownloadID, TextureCounter, TextureBuffer));
			}
			}else{
			TextureBuffer[TextureResetCounter] = blockTextureData[i];
			}
		}
		TextureBlockFragments = TextureCounter;	
	}
	public Packet GetDownloadPacket(){
		ByteBuffer ReturnArr = ByteBuffer.allocate(48);//48 Bits 4+4+8+16+16
		ReturnArr.putInt(ModelBlockFragments);
		ReturnArr.putInt(TextureBlockFragments);
		ReturnArr.putInt(BlockID);
		ReturnArr.putLong(DownloadID);
		ReturnArr.put(ModelMD5Checksum);
		ReturnArr.put(TextureMD5Checksum);
		return new Packet(PackCode, new PacketDescriptor().setPacketDesc(Descriptor),
				(short)ReturnArr.array().length, ReturnArr.array());
	}
	public ArrayList<Packet> getModelPackets(){
		ArrayList<Packet> returnPackets = new ArrayList<Packet>();
		for (int i=0;i <= ModelList.size(); i++){
			returnPackets.add(ModelList.get(i).GetPacket());
		}
		return returnPackets;
	}
	public ArrayList<Packet> getTexturePackets(){
		ArrayList<Packet> returnPackets = new ArrayList<Packet>();
		for (int i=0;i<=TextureList.size();i++){
			returnPackets.add(TextureList.get(i).GetPacket());
		}
		return returnPackets;
	}
	public String getHMM(){
		return "HMM_OO104\n" +
				"Build\n";
	}
	public String getHashType(){
		return "MD5";
	}
	public boolean getPackManOOM(){
		return true;
	}
}
