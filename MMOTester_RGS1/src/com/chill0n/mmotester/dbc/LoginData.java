package com.chill0n.mmotester.dbc;

public class LoginData {
	private String LoginUsername;
	private String LoginPasswordHash;
	private long LoginPLID;
	public LoginData(String Username, String PasswordHash, long plid){
		LoginUsername = Username;
		LoginPasswordHash = PasswordHash;
		LoginPLID = plid;
	}
	public String getUsername(){
		return LoginUsername;
	}
	public String getPasswordHash(){
		return LoginPasswordHash;
	}
	public long getPLID(){
		return LoginPLID;
	}
}
