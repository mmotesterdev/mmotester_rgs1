/**
 * 
 */
package com.chill0n.mmotester.player;

/**
 * @author nwhiteis
 *
 */
public class PlayerCoordinate {
	private float X_coord, Y_coord, Z_coord, X_delta, Y_delta, Z_delta;
	public long plid, send_time;
	public PlayerCoordinate(long plid, long sent_time,float x_coord, float y_coord, float z_coord, float x_delta, 
			float y_delta, float z_delta){
		X_coord = x_coord;
		Y_coord = y_coord;
		Z_coord = z_coord;
		X_delta = x_delta;
		Y_delta = y_delta;
		Z_delta = z_delta;
	}
	public void Update_X(float x_coord){
		X_coord = x_coord;
	}
	public void Update_Y(float y_coord){
		Y_coord = y_coord;
	}
	public void Update_Z(float z_coord){
		Z_coord = z_coord;
	}
	public void UpdateAll(float x_coord, float y_coord, float z_coord, float x_delta, float y_delta,
			float z_delta){
		X_coord = x_coord;
		Y_coord = y_coord;
		Z_coord = z_coord;
		X_delta = x_delta;
		Y_delta = y_delta;
		Z_delta = z_delta;
	}
	public void Update_X_Delta(float x_delta){
		X_delta = x_delta;
	}
	public void Update_Y_Delta(float y_delta){
		Y_delta = y_delta;
	}
	public void Update_Z_Delta(float z_delta){
		Z_delta = z_delta;
	}
	public float Get_X_Delta(){
		return X_delta;
	}
	public float Get_Y_Delta(){
		return Y_delta;
	}
	public float Get_Z_Delta(){
		return Z_delta;
	}
	public float Get_X_Coord(){
		return X_coord;
	}
	public float Get_Y_Coord(){
		return Y_coord;
	}
	public float Get_Z_Coord(){
		return  Z_coord;
	}
}
