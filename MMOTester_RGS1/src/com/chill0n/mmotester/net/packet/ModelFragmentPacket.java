package com.chill0n.mmotester.net.packet;

import java.nio.ByteBuffer;

import com.chill0n.mmotester.net.Packet;
import com.chill0n.mmotester.net.PacketDescriptor;

public class ModelFragmentPacket {
	public final int PackCode = 8;
	public final int Descriptor = 2; //TCP
	private Packet ReturnPacket;
	public ModelFragmentPacket(long DownLoadID, int FragmentNumber ,byte Data[]){
		ByteBuffer AddBuffer = ByteBuffer.allocate(12+Data.length);
		AddBuffer.putLong(DownLoadID);
		AddBuffer.putInt(FragmentNumber);
		AddBuffer.put(Data);
		ReturnPacket = new Packet(PackCode, new PacketDescriptor().setPacketDesc(Descriptor),
				(short)20001, AddBuffer.array());
	}
	public Packet GetPacket(){
		return ReturnPacket;
	}
}
