package com.chill0n.mmotester.constants;

public class Constants {
	public static final boolean isDebugMode = false;
	public static final int BuildNumInt=00000;
	public static final String isDebugString = "DEBUG";
	public static final byte DebugRadomKey[] = {0xf}; //15 In Decimal
	public static final long DebugAcceptablePLID[] = {1,2,3,4}; //Allow 1,2,4,5
	public static final int DebugThreadPoolSize = 4;
	public static final String BuildNum = "00000";
	public static final String VVersion = "0-0-1D";
	public static final String Copyright = "Copyright Chill0n LLC All Rights Reserved";
	public static final String BuildEmp = "Noah Whiteis";
	public static final int DBCPort = 27017;
	public static final String DBCURL = "localhost";
	public static final String DBCName = "mmousers";
	public static final String DBCUsrname = "admin";
	public static final String DBCPassword = "admin";
	public static final String DBCollectioName = "usercollection";
	public static final String FlatURL = "ccs-access.com/accmgr/"; //JSON Account Request URL
	public static final String LoginACK = "ACK";
	public static final int UDPOffset = 5000;
	public static final int ServerListenTimeout = 10000;
	public static final int MaxPacketSize = 4046;
	public static final int TCPPort = 30456;
	public static final int DebugConsolePort = 30293;
	public static final boolean usePassword=true;
	public static final String ConsolePassword = "12345";
	public static final String ConsoleUsername = "main";
	public static final String LoginGood="GOOD!";
	public static final String LoginBad="BAD/PASS?";
	public static final String DBCBlockCollectionName = "blocks";
	public static final String DBCBlockDatabaseName = "blockdb";
	public static final String DBCBlockIP = "localhost";
	public static final int DBCBlockPort = 27017;
}
