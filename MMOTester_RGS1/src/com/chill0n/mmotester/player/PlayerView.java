package com.chill0n.mmotester.player;

public class PlayerView {
	protected float Roll, Yaw, Pitch;
	protected long Plid;
	public PlayerView(long plid, float roll, float yaw, float pitch){
		Roll = roll;
		Yaw = yaw;
		Pitch = pitch;
		Plid = plid;
	}
	public long getPLID(){
		return Plid;
	}
	public PlayerView setRoll(float roll){
		Roll = roll;
		return this;
	}
	public PlayerView setYaw(float yaw){
		Yaw = yaw;
		return this;
	}
	public PlayerView setPitch(float pitch){
		Pitch = pitch;
		return this;
	}
	public float getRoll(){
		return Roll;
	}
	public float getYaw(){
		return Yaw;
	}
	public float getPitch(){
		return Pitch;
	}
}
