package com.chill0n.mmotester.net.packet;

import java.nio.ByteBuffer;

import com.chill0n.mmotester.net.Packet;
import com.chill0n.mmotester.net.PacketDescriptor;

public class TimeUpdatePacket {
	private Packet PacketRetPacket;
	public final int PackCode = 5;
	public final int Descriptor = 2; //TCP
	public TimeUpdatePacket(){
		ByteBuffer Time = ByteBuffer.allocate(8);
		Time.putLong(System.nanoTime());
		PacketRetPacket = new Packet(PackCode, new PacketDescriptor().setPacketDesc(Descriptor), 
				(short)8, Time.array());
	}
	public Packet getPacket(){
		return PacketRetPacket;
	}
}
