package com.chill0n.mmotester.net.packet;

import java.nio.ByteBuffer;

import com.chill0n.mmotester.net.Packet;
import com.chill0n.mmotester.net.PacketDescriptor;
import com.chill0n.mmotester.player.PlayerData;

public class PlayerUpdatePacket implements BasePacket{
	public final int PackCode = 4;
	public final int Descriptor = 2;
	private ByteBuffer RawUpdate;
	public PlayerUpdatePacket(PlayerData plHandle){
		RawUpdate = ByteBuffer.allocate(64+(plHandle.OwnedBlocks.size())*8+(plHandle.OwnedItems.size()*8)
				+(plHandle.PlayerName.getBytes().length)); //Allocate This Son Of A Bit**
		RawUpdate.putInt(plHandle.OwnedBlocks.size()); //Amount Of Owned Blocks; 4
		RawUpdate.putInt(plHandle.OwnedItems.size()); //Amount Of Owned Items; 4;
		RawUpdate.putInt(plHandle.PlayerName.getBytes().length); // Player Name; 4
		RawUpdate.putInt(plHandle.CurBlock); //Current Block (32 Bit Integer); 4
		RawUpdate.putFloat(plHandle.CurCoordinates.Get_X_Coord()); //X Coordinate (Float); 4
		RawUpdate.putFloat(plHandle.CurCoordinates.Get_Y_Coord()); //Y Coordinate (Float); 4
		RawUpdate.putFloat(plHandle.CurCoordinates.Get_Z_Coord()); //Z Coordinate (Float); 4
		RawUpdate.putInt(plHandle.PlayerClass); // Player Class (32 Bit Integer); 4
		RawUpdate.putInt(plHandle.PlayerLevel); // Player Level (32 Bit Integer); 4
		RawUpdate.putInt(plHandle.PlayerState); // Player State (32 But Integer); 4
		RawUpdate.putDouble(plHandle.CurrentMoney); //Players Current Currency (Double 64bit); 8
		for(int i=0;i<=plHandle.OwnedBlocks.size();i++){
			RawUpdate.putLong(plHandle.OwnedBlocks.get(i)); // Owned Block (Long); 8*amount
		}
		for(int i=0;i<=plHandle.OwnedItems.size();i++){
			RawUpdate.putLong(plHandle.OwnedItems.get(i)); // Owned Items (Long); 8*amount
		}
		RawUpdate.putLong(plHandle.PlayerGuild); // Player Guild (Long); 8
		RawUpdate.putLong(plHandle.PlayerID); // Player ID (Long); 8
		RawUpdate.put(plHandle.PlayerName.getBytes()); //Player Name 
	}
	@Override
	public Packet getPacket() {
		Packet TempPacket = new Packet(PackCode, new PacketDescriptor().setPacketDesc(Descriptor), 
				(short)RawUpdate.capacity(), RawUpdate.array());
		return TempPacket;
	}

}
