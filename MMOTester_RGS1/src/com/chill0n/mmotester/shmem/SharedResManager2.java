package com.chill0n.mmotester.shmem;

import java.net.UnknownHostException;
import java.util.Vector;

import com.chill0n.mmotester.dbc.DBCController;
import com.chill0n.mmotester.world.Block;

public class SharedResManager2 {
	static Vector<Block> BlockData;
	public static void SynchronizeMemoryMap_USEONLYONCE(){
		BlockData = new Vector<Block>();
	}
	public static void BuildBlockMemoryMap_USEONLYONCE(){
		try { //Run After SynchronizeMemoryMap
			System.out.println("[Info]Preparing And Loading Block Metadata Block Table, Entries:"+
					new DBCController().GetTotalNumOfBlocks());
			for(int i=0;i<(new DBCController().GetTotalNumOfBlocks());i++){
				BlockData.add(new DBCController().DownloadBlock(i+1));
			}// Well that was easy
			System.out.println("[Info]Processed Block Requests ["+BlockData.size()+"]");
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			//Im going to allow this since this is only ran at startup:>
			e.printStackTrace();
		}
	}
	public static synchronized Block GetBlock(long blockid){
		return BlockData.get((int) blockid);
	}
}
